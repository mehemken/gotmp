// cmd::lib
package cmd

import (
	"encoding/csv"
	"errors"
	"fmt"
	"gopkg.in/yaml.v3"
	"io"
	"io/ioutil"
	"log"
	"strings"
	"text/template"

	"github.com/Masterminds/sprig"
)

////////////////////////////////////////////////////////////
// funcs
////////////////////////////////////////////////////////////

// get_yaml_template: Gets template file from flag
func get_template() (*template.Template, error) {

	// Get the flag values
	template_from_flag := rootCmd.PersistentFlags().Lookup("template").Value.String()

	// Read in the template file
	//   This needs to support a directory value.
	//   And downstream will probably also need to
	//   support it.
	f, err := ioutil.ReadFile(template_from_flag)
	if err != nil {
		msg := fmt.Sprintf("error: %v", err)
		return template.New("nil"), errors.New(msg)
	}

	// Instantiate template
	//   - including the Sprig library
	t, err := template.New("output").Funcs(sprig.TxtFuncMap()).Parse(string(f))
	if err != nil {
		msg := fmt.Sprintf("error: %v", err)
		return template.New("nil"), errors.New(msg)
	}

	return t, nil
}

// get_values: gets the user provided values
func get_values() (UserValues, error) {

	// Get the flag values
	filetype_from_flag := rootCmd.PersistentFlags().Lookup("filetype").Value.String()

	// Values
	var user_values UserValues

	// Check the filetype
	switch filetype_from_flag {
	case "yaml":
		user_values = CreateYAML()
		err := user_values.Populate()
		if err != nil {
			msg := fmt.Sprintf("filetype error: %v", err)
			return user_values, errors.New(msg)
		}
		log.Printf("user_values: %v\n", user_values)
	case "csv":
		user_values = CreateCSV()
		err := user_values.Populate()
		if err != nil {
			msg := fmt.Sprintf("filetype error: %v", err)
			return user_values, errors.New(msg)
		}
		log.Printf("user_values: %v\n", user_values)
	default:
		msg := fmt.Sprintf("filetype: [%v] is invalid", filetype_from_flag)
		return user_values, errors.New(msg)
	}

	return user_values, nil
}

////////////////////////////////////////////////////////////
// Types
////////////////////////////////////////////////////////////

type UserValues interface {
	Populate() error
}

//////////// Yaml

// YAMLValues: The values that will populate the template
type YAMLValues struct {
	Values map[interface{}]interface{}
}

// CreateYAML: Creates a YAMLValues
func CreateYAML() *YAMLValues {
	return &YAMLValues{}
}

// Populate: Takes yaml from the given file and puts it into a struct
func (y *YAMLValues) Populate() error {
	// File from flag
	file_from_flag := rootCmd.PersistentFlags().Lookup("file").Value.String()

	// Read data from yaml to a map
	values_yaml, err := ioutil.ReadFile(file_from_flag)
	if err != nil {
		msg := fmt.Sprintf("error: %v", err)
		return errors.New(msg)
	}
	yaml.Unmarshal(values_yaml, &y.Values)

	return nil
}

//////////// CSV

// CSVValues: The values that will populate the template
type CSVValues struct {
	Values []map[string]string
}

// CreateCSV: Creates a CSVValues
func CreateCSV() *CSVValues {
	return &CSVValues{}
}

// Populate: takes data from csv and makes a CSVConf
func (c *CSVValues) Populate() error {

	// Get the flag values for file and template
	file_from_flag := rootCmd.PersistentFlags().Lookup("file").Value.String()

	// Read in data from csv
	values_csv, err := ioutil.ReadFile(file_from_flag)
	if err != nil {
		msg := fmt.Sprintf("error: %v\n", err)
		return errors.New(msg)
	}
	reader := csv.NewReader(strings.NewReader(string(values_csv)))

	// Populate the struct
	var header_row []string

	for i := 0; ; i++ {

		// Read a line
		record, err := reader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			msg := fmt.Sprintf("error: %v\n", err)
			return errors.New(msg)
		}

		// This is the header row. Save these values for later
		if i == 0 {
			header_row = record
			continue
		}

		// Populate c
		new_record := make(map[string]string)
		for i, key := range header_row {
			new_record[key] = record[i]
		}
		c.Values = append(c.Values, new_record)
	}

	return nil
}
