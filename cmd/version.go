// cmd::version
package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(versionCmd)
}

const version = "0.2.0"

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the current version of gofmt.",
	Long:  "Print the current version of gofmt.",
	Run:   func(cmd *cobra.Command, args []string) {
        fmt.Printf("gotmp v%s\n", version)
    },
}

