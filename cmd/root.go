// cmd::root
package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"os"
)

////////////////////////////////////////////////////////////
// Init
////////////////////////////////////////////////////////////

func init() {
	var File string
	var Template string
    var Filetype string

    // func StringVarP: func StringVarP(p *string, name, shorthand string, value string, usage string)
	rootCmd.PersistentFlags().StringVarP(&File, "file", "f", "values.yaml", "Supply the name of the values file you wish to use.")
	rootCmd.PersistentFlags().StringVarP(&Template, "template", "t", "", "Supply the name of the template file you wish to use.")
	rootCmd.PersistentFlags().StringVarP(&Filetype, "filetype", "y", "yaml", "Supply the type of file of the values. [yaml|csv]")
}

var rootCmd = &cobra.Command{
	Use:   "gotmp",
	Short: "Gotmp is like helm templates for terraform: hellaform",
	Long: `Using source values in a csv file and a target template,
    generate output with the same syntax and functions available in
    Helm!`,
	Run: func(cmd *cobra.Command, args []string) {
        fmt.Println("This is the root command")
    },
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
