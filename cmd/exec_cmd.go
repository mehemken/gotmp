// cmd::exec
package cmd

import (
	"log"
	"os"

	"github.com/spf13/cobra"
)

func init() {
	execCmd := &cobra.Command{
		Use:   "exec",
		Short: "Populate a template based on a custom values.",
		Long:  "Create a csv or yaml file with all the values you need and populate the template file with those values.",
		Run:   command_exec,
	}

	rootCmd.AddCommand(execCmd)
}

////////////////////////////////////////////////////////////
// Funcs
////////////////////////////////////////////////////////////

func command_exec(cmd *cobra.Command, args []string) {

	// Get template
	exec_template, err := get_template()
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}

	// Get data
	exec_values, err := get_values()
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}

	// Execute template
	err = exec_template.Execute(os.Stdout, exec_values)
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}

}
