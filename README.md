# Go Templates

This project came out of a frustration with Terraform. Terraform is a configuration syntax that aspires to be a programming language. It doesn't have proper looping or conditional statements. This makes it very tedious to create resources that are very similar to each other without repeating code. This project aims to separate the resource definitions from parameters. It allows you to treat Terraform resources a little bit like Helm treats Kubernetes resources. In fact, both Helm and Gotmp use go templates and the [Sprig library](https://github.com/Masterminds/sprig) to accomplish powerful templating.

## How it works

You can populate a template with either YAML or from a CSV file.

    gotmp [yaml|csv] -f [VALUES_FILE] -t [TEMPLATE_FILE]

*Explanation* The base command will be named `gotmp` (tentative, I rather like "hellaform"). There are two subcommands: `yaml` and `csv`. If your values are in YAML then you would use the `yaml` subcommand and if your values are in CSV you would use the `csv` subcommand. The `TEMPLATE_FILE` will be a Go text template file ([example][1]).


### YAML

Initial use case is to generate the necessary files for db instances in terraform. Those values lend themselves to be in YAML because there are not endless individual instances (in contrast with security group rules). Thus, this command should suffice.

    gotmp yaml -f values.yaml -t example.tf

Here's an example file:

    // values.yaml
    name: Jon Snow
    house: Stark
    weapons:
      - weapon: Sword
        material: Valyrian_Steel
      - weapon: Dagger
        material: Iron

And an example template:

    // example.tf
    data "aws_vpc" "{{ .Values.name }}" {
        id = "vpc-98dhaf9s8df"
        tags {
            {{- range .Values.weapons }}
            "{{ .weapon }}" = "{{ .material | lower }}"
            {{- end }}
        }
    }

Results in:

    data "aws_vpc" "Jon Snow" {
        id = "vpc-98dhaf9s8df"
        tags {
            "Sword" = "valyrian_steel"
            "Dagger" = "iron"
        }
    }

### CSV

The initial use case for a CSV values file is the security group rules. AWS forces an endless list of security group rules by design. Doing those by hand is tedious and boring. Translating each one from the Architect's spreadsheet into Terraform is not just slow and error prone. It is a 1:1 input:output state of affairs. One rule in the spreadsheet must produce one and only one correct rule in terraform. That sounds like something that is better done by a machine than a human. Thus, this command should suffice.

    gotmp csv -f values.csv -t template.tf

Here's an example `values.csv` file:

    Name,Desc,Meta
    Lanister,Debts,PayThemBack
    Stark,Winter,IsComing
    Frey,RedWedding,DidThatJustHappen

And here is an example `template.tf` file:

    {{ range .Values }}
    data "aws_vpc" "{{ .Name }}" {
        id = "vpc-98dhaf9s8df"
        tags {
            "{{ .Desc }}" = "{{ .Meta | lower }}"
        }
    }
    {{ end }}

Results in:

    data "aws_vpc" "Lanister" {
        id = "vpc-98dhaf9s8df"
        tags {
            "Debts" = "paythemback"
        }
    }

    data "aws_vpc" "Stark" {
        id = "vpc-98dhaf9s8df"
        tags {
            "Winter" = "iscoming"
        }
    }

    data "aws_vpc" "Frey" {
        id = "vpc-98dhaf9s8df"
        tags {
            "RedWedding" = "didthatjusthappen"
        }
    }


---

[1]: https://dlintw.github.io/gobyexample/public/text-template.html "Text template example"
